const fs = require('fs');

let fetchNotes = () => {
    //Since the file may not exists at first we need to put the readFileSync in a try catch
    try {
        let notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
    } catch (e) {
        return [];
    }
};

let saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {

    let notes = fetchNotes();
    let note = {
        title,
        body
    };

    //To check if we have duplicate title - we will be allowing only unique titles
    //ES6 allows this syntax to only return notes that match the current title
    let duplicateNotes = notes.filter((note) => note.title === title);
    if (duplicateNotes.length === 0) {
        notes.push(note);
        saveNotes(notes);
        return note;
    }

    //If we do not add a return statement here, undefined is returned by default in javascript

};

var getAll = () => {
    return fetchNotes();
};

var getNote = (title) => {
    let notes = fetchNotes();
    let notesFiltered = notes.filter((note) => note.title === title);
    return notesFiltered.length ? notesFiltered[0] : false;
};

var removeNote = (title) => {
    let notes = fetchNotes();
    //ES6 allows this syntax where it returns all notes that do not match the title
    let filteredNotes = notes.filter((note) => note.title !== title);
    saveNotes(filteredNotes);
    return filteredNotes.length !== notes.length;
};

var logNote = (message, note) => {
    //Break on this line and use repl to output note
    debugger;
    console.log(message);
    console.log('----');
    console.log(`Title: ${note.title}`);
    console.log(`Body: ${note.body}`);
};

module.exports = {
    addNote, //ES6 shortcut same as 'addNote: addNote()' since both are identical
    getAll,
    getNote,
    removeNote,
    logNote
};