const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

const options = {
    title: {
        describe: 'Title of Note',
        demand: true,
        alias: 't'
    },
    body: {
        describe: 'Body of Note',
        demand: true,
        alias: 'b'
    }
};

//Outputs all arguments from cli: e.g. node app.js list : we will have access to list
const argv = yargs
    .command('add', 'Add a new note', {
        title: options.title,
        body: options.body
    })
    .command('list', 'List all notes')
    .command('read', 'Read a single note', {
        title: options.title
    })
    .command('remove', 'Remove a note by title', {
        title: options.title
    })
    .help()
    .argv;
let command = argv._[0];

switch (command) {
    case 'add':
        //node app.js add --title="Title Here" --body="Body Here"
        let note = notes.addNote(argv.title, argv.body);
        if (note) {
            notes.logNote('Note Created Successfully', note);
        } else {
            console.log('Note Title Taken.')
        }
        break;
    case 'list':
        let allNotes = notes.getAll();
        console.log(`Printing ${allNotes.length} note(s):`);
        allNotes.forEach((note) => notes.logNote('', note));
        break;
    case 'read':
        //node app.js read --title="Title Here"
        let noteRead = notes.getNote(argv.title);
        if (noteRead) {
            notes.logNote('Note Read Successfully', noteRead);
        } else {
            console.log('Note not found');
        }
        break;
    case 'remove':
        //node app.js remove --title="Title Here"
        let removed = notes.removeNote(argv.title);
        let message = removed ? 'Note removed successfully' : 'Note not found';
        console.log(message);
        break;
    default:
        console.log("Command not recognized");
}