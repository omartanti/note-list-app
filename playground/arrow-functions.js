// let square = (x) => x * x;
let square = x => x * x; //When having one parameter you can ommit the brackets
console.log(square(9));

let user = {
    name: 'Omar',
    sayHi: () => { //ES6 arrow function
        console.log(arguments);//This print an object of the global scope
        console.log(`Hi. I'm ${this.name}`); //This print 'Hi. I'm undefined'
    },
    sayHiAlt () { //Normal function
        console.log(arguments); //This print an object with all the arguments passed when calling the function
        console.log(`Hi. I'm ${this.name}`); //This print 'Hi. I'm Omar'
    }
};
user.sayHi(1, 2, 3, 4);
user.sayHiAlt(1, 2, 3, 4);